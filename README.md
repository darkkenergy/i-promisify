# IPromisify

> Transforms a function into a promise, even when the function's callback is not the last argument. This module is build upon promise.js (https://github.com/stackp/promisejs), which offers the ".then" method.

## Install

```bash
npm i i-promisify -S
```

## Inclusion

```js
var IPromisify = require('i-promisify');
```

## Usage

```js
/**
 * @name IPromisify
 * @version 1.0.1
 * @author Claudio Nuñez Jr.
 * @desc Transforms a function into a promise, even when the function's callback
 *      is not the last argument. This module is built upon promise.js
 *      (https://github.com/stackp/promisejs), which offers the ".then" method.
 * 
 * @param thing {mixed} :If "thing" is a function, wrap the function in a promise
 *      API, if "thing" is already a promise, return it as is, otherwise, return
 *      (resolve) the "thing" immediately.
 * @param callbackIndex {number} :[Optional][Default::-1 (last argument)]
 *      Argument index of where the resolving callback lives.
 * @param context {object} :[Optional] The context (scope) to pass to the
 *      @param::thing.
 */

// IPromisify(thing, callbackIndex, context)

var IPromisify = require('i-promisify');

// Will hold some asyncronous function.
var someAsyncFn;

// Will hold the promisified asyncronous function.
var someAsyncFnPromise;

// A callback that we will pass to the asyncronous function.
var callback = function(asyncResponse) {
    return asyncResponse;
};

// Set up the promise resolve-handler.
var resolveHandler = function(error, asyncResponse) {
    if (!error) {
        
        // Outputs the async response.
        console.log("Return value from callback: ", asyncResponse);
    }
};
```

### Example 1: Promisify some async function that takes a callback as it's last parameter.

```js
someAsyncFn = function(val, callback) {
    // Do some async thing & run the callback when it's done.
};

// No need to set the callback index since the callback will already be at the
// argument-list's last index.
someAsyncFnPromise = IPromisify(someAsyncFn);

// Register the promise resolve-handler.
someAsyncFnPromise.then(resolveHandler);

// Execute the promisifed function exactly how you would for someAsyncFn.
someAsyncFnPromise("some value", callback);
```

### Example 2: Promisify some async function that takes a callback as it's second parameter, & pass it a scope.

```js
someAsyncFn = function(val, callback, otherVal) {
    
    // Outputs "my scope"
    console.log("my ", this.my);
    
    // Do some async thing & run the callback when it's done.
};

// Set the callback index to the second position, index 1, to match the
// asyncronous function's parameter signature, & set a context (scope.)
someAsyncFnPromise = IPromisify(someAsyncFn, 1, {
    my: "scope"
});

// Register the promise resolve-handler.
someAsyncFnPromise.then(resolveHandler);

// Execute the promisifed function exactly how you would for someAsyncFn.
someAsyncFnPromise("some value", callback, "other value");
```

### Example 3: Return a promise from the resolve-handler to chain another resolve-handler.

```js
someAsyncFn = function(callback) {
    // Do some async thing & run the callback when it's done.
};

// someAsynFn promise transformation.
someAsyncFnPromise = IPromisify(someAsyncFn);

// Set up the promise resolve-handlers.
someAsyncFnPromise.then(function(error, asyncResponse) {
    if (!error) {
        return someAsyncFnPromise(callback);
    }
})
.then(function(error, newAsyncResponse) {
    // Do more stuff...
});

// Execute the promisifed function exactly how you would for someAsyncFn.
someAsyncFnPromise(callback);
```

### Example 4: Promisify some object (any type other than a function).

```js
var someObject = {
    some: "object"
};

// Transform some object into a promise.
var someObjectPromise = IPromisify(someObject);

// Set up the resolve-handler.
someObjectPromise.then(function(error, someObject) {
    
    // The resolve-handler will return the original object immediately.
    // error is false.
    
    // Outputs "some object"
    console.log("some ", someObject.some);
});
```