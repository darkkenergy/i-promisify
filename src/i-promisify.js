var Promise = require('promise.js');

/**
 * @name IPromisify
 * @version 1.0.1
 * @author Claudio Nuñez Jr.
 * @desc Transforms a function into a promise, even when the function's callback
 *      is not the last argument. This module is built upon promise.js
 *      (https://github.com/stackp/promisejs), which offers the ".then" method.
 * 
 * @param thing {mixed} :If "thing" is a function, wrap the function in a promise
 *      API, if "thing" is already a promise, return it as is, otherwise, return
 *      (resolve) the "thing" immediately.
 * @param callbackIndex {number} :[Optional][Default::-1 (last argument)]
 *      Argument index of where the resolving callback lives.
 * @param context {object} :[Optional] The context (scope) to pass to the
 *      @param::thing.
 */
function IPromisify(thing, callbackIndex, context) {
    var promisified = function() {
        var promise,
            args = Array.prototype.slice.call(arguments);

        if (typeof thing === "object" && thing.then) {

            // The thing is already a promise.
            promise = thing;
        } else {
            promise = new Promise.Promise();

            if (typeof thing === "function") {
                
                // The thing is a function.
                
                var thingCallback, callback;
                
                // Take the callback out of the argument list so we can work it.
                callbackIndex = (callbackIndex || callbackIndex === 0 ? callbackIndex : -1);
                thingCallback = args.splice(callbackIndex, 1)[0];
                
                // Wraps the thing's callback, so we can resolve the promise with
                // the callback's return value.
                callback = function() {
                    var args = [false].concat(Array.prototype.slice.call(arguments));
                    promise.done(false, thingCallback.apply(this, args));
                };
                
                // Insert the wrapped callback where it was, back into the
                // argument list.
                args.splice(callbackIndex, 0, callback);
                
                try {
                    thing.apply(context, (args || []));
                } catch(e) {
                    
                    // Resolve with (error=true, exception)
                    promise.done(true, e);
                }
            } else {
                
                // The thing was not a function.
                // Resolve with (error=false, original thing)
                promise.done(false, thing);
            }
        }

        return promise;
    };
    
    return promisified;
}

module.exports = IPromisify;